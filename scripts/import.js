// Imports
const firestoreService = require('firestore-export-import');
const serviceAccount = require('./data/serviceAccount.json');

const databaseURL = "https://matesze-hu.firebaseio.com";

const opts = {
  dates: ['birthday'],
  geos: [],
  refs: [],
};

// JSON To Firestore
const jsonToFirestore = async () => {
  try {
    console.log('Initialzing Firebase');
    await firestoreService.initializeApp(serviceAccount, databaseURL);
    console.log('Firebase Initialized');

    await firestoreService.restore('./data/dogs.json', opts);
    await firestoreService.restore('./data/organizations.json', opts);
    await firestoreService.restore('./data/persons.json', opts);
    console.log('Upload Success');
  }
  catch (error) {
    console.log(error);
  }
};

jsonToFirestore();
