import {Component, OnInit} from '@angular/core';
import {IOrganization} from '../domain';
import {OrganizationsService} from '../organizations.service';

@Component({
  selector: 'app-organizations',
  templateUrl: './organizations.component.html',
  styleUrls: ['./organizations.component.css']
})
export class OrganizationsComponent implements OnInit {

  organizations: IOrganization[];
  organization: IOrganization;

  constructor(private organizationsService: OrganizationsService) {
  }

  ngOnInit(): void {
    this.loadOrganizations();
  }

  loadOrganizations() {
    this.organizationsService.organizations.subscribe(o => this.organizations = o);
  }

  onSelect(org: IOrganization) {
    this.organization = org;
  }
}
