import {Injectable} from '@angular/core';
import {Sex} from './domain';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';

export interface IPetVetRecord {
  found: boolean;
  recordUrl: string;
  chipNo: string;
  tattooNo: string;
  name: string;
  kind: string;
  breed: string;
  sex: Sex;
  lastModification: Date;
  dangerous: boolean;
  lastVaccination: string;
  passedAway: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class PetvetService {


  constructor(private httpClient: HttpClient) {
  }

  getPetVetRecordByChipNo(chipNo: string): Observable<IPetVetRecord> {
    if ('900032000480488' === chipNo) {
      return of({
        found: true,
        recordUrl: 'https://www.petvetdata.hu/keres?search-number=mikrochip&search-value=900032000480488',
        chipNo: '900032000480488',
        tattooNo: null,
        name: 'Oszkár',
        kind: 'kutya',
        breed: 'Közép uszkár',
        sex: Sex.Male,
        lastModification: null,
        dangerous: false,
        lastVaccination: 'Intervet NobivacDHPPi+RL, 2019.06.20',
        passedAway: false
      });
    } else {
      return of({
        found: false,
        recordUrl: 'https://www.petvetdata.hu/keres?search-number=mikrochip&search-value=' + chipNo,
        chipNo: null,
        tattooNo: null,
        name: null,
        kind: null,
        breed: null,
        sex: null,
        lastModification: null,
        dangerous: null,
        lastVaccination: null,
        passedAway: null
      });
    }
  }
}
