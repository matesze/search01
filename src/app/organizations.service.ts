import {Injectable} from '@angular/core';
import {IDog, IOrganization, IPerson} from './domain';
import {Observable} from 'rxjs';
import {AngularFirestore} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class OrganizationsService {

  organizations: Observable<IOrganization[]>;
  persons: Observable<IPerson[]>;
  dogs: Observable<IDog[]>;

  constructor(firestore: AngularFirestore) {
    this.organizations = firestore.collection<IOrganization>('organizations').valueChanges();
    this.persons = firestore.collection<IPerson>('persons').valueChanges();
    this.dogs = firestore.collection<IDog>('dogs').valueChanges();
  }
}
