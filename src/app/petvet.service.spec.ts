import { TestBed } from '@angular/core/testing';

import { PetvetService } from './petvet.service';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';
import {AppModule} from './app.module';

describe('PetvetService', () => {
  let service: PetvetService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        AppModule
      ]
    }).compileComponents();
    service = TestBed.inject(PetvetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
