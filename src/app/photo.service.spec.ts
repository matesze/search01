import { TestBed } from '@angular/core/testing';

import { PhotoService } from './photo.service';
import {RouterTestingModule} from '@angular/router/testing';
import {AppModule} from './app.module';

describe('PhotoService', () => {
  let service: PhotoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        AppModule
      ]
    }).compileComponents();
    service = TestBed.inject(PhotoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
