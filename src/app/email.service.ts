import {Injectable} from '@angular/core';
import {IPerson} from './domain';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  constructor() {
  }

  isValidatedEmail(email: string): Observable<boolean> {
    return of(true);
  }

  validatePersonEmail(person: IPerson): void {
    if (this.isValidatedEmail(person.email)) {
      return; // do nothing
    }
    // send out email with link
    // hook up validatedEmails with new row
    // when link clicked, record/update the row
  }
}
