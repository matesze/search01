import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {IDog, IPerson} from './domain';

export interface IPhoto {
  found: boolean;
  photoUrl: string;
  altText: string;
  type: string;
  typeId: string;
}

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  constructor(private httpClient: HttpClient) {
  }

  getPersonPhoto(person: IPerson): Observable<IPhoto> {
    return of({
      found: true,
      photoUrl: 'http://www.fillmurray.com/200/300',
      altText: `${person.lastName}`,
      type: 'person',
      typeId: person.id
    });
  }

  getDogPhoto(dog: IDog): Observable<IPhoto> {
    return of({
      found: true,
      photoUrl: 'https://upload.wikimedia.org/wikipedia/commons/3/34/Labrador_on_Quantock_%282175262184%29.jpg',
      altText: `${dog.callName}`,
      type: 'dog',
      typeId: dog.id
    });
  }
}
