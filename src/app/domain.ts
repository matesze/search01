interface IEntity {
  id: string;
  active: boolean;
  created: Date;
  modified: Date;
}

export interface IGeoLocation {
  latitude: number;
  longitude: number;
  altitude: number;
}

export interface IAddress {
  isoCountryCode: string;
  zipCode: string;
  city: string;
  street: string;
  houseNo: string;
  ext: string;
  geolocation: IGeoLocation;
}

export interface IPostalAddress extends IAddress{
  name: string;
}

export const enum Sex {
  Male = 'm',
  Female = 'f'
}

// Hallássérülteket segítő -- Hearing Dog
// Mozgássérült-segítő -- Mobility Dog
// Rohamjelző -- Seizure Dog
// Személyi segítő -- Support Dog
// Terápiás -- Therapy Dog
// Vakvezető -- Guide Dog

export const enum DogType {
  Hearing = 'hearing',
  Mobility = 'mobility',
  Seizure = 'seizure',
  Support = 'support',
  Therapy = 'therapy',
  Guide = 'guide'
}

export const enum PersonRole {
  OrgRepresentative = 'orgRepresentative',
  OrgContact = 'orgContact',
  Trainer = 'trainer',
  User = 'user',
  OrgExaminer = 'orgExaminer',
  Examiner1 = 'examiner1',
  Examiner2 = 'examiner2'
}

export interface IDogTraining {
  dogType: DogType;
  dogTrainer: IPerson;
  dogOrganization: IOrganization;
}

export interface IPersonRole {
  personRole: PersonRole;
  organization: IOrganization;
}

export interface IOrganization extends IEntity {
  shortName: string;
  fullName: string;
  siteUrl: string;
  email: string;
  hqAddress: IAddress; // where is officially registered HQ
  mailingAddress: IPostalAddress; // where mail should be sent, if any
  documents: DogType[];
  dogTypes: string[];
  representativePerson: IPerson;
  contactPerson: IPerson;
  mainTrainer: IPerson;
}

export interface IDog extends IEntity {
  callName: string;
  chipNo: string;
  birthday: Date;
  breed: string;
  sex: Sex;
  neutered: boolean;
  training: IDogTraining[];
}

export interface IPerson extends IEntity {
  title: string; // "dr." or null
  lastName: string;
  firstName: string;
  motherMaidenName: string;
  birthday: Date;
  email: string;
  phone: string;
}

export interface ILocation {
  shortName: string;
  address: IAddress;
  geolocation: IGeoLocation;
}

export interface IDocument extends IEntity {
  originalName: string;
  uploader: string;
  uploaded: Date;
}

export interface ICertificate extends IEntity {
  identifier: string;
  dog: IDog;
  person: IPerson;
  type: DogType;
  issued: Date;
  validUntil: Date;
}

export interface IExam extends IEntity {
  dogType: DogType;
  location: ILocation;
}
