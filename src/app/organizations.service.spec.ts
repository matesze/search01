import { TestBed } from '@angular/core/testing';

import { OrganizationsService } from './organizations.service';
import {RouterTestingModule} from '@angular/router/testing';
import {AppModule} from './app.module';

describe('OrganizationsService', () => {
  let service: OrganizationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        AppModule
      ]
    }).compileComponents();
    service = TestBed.inject(OrganizationsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
