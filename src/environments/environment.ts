// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDnbUWx1nZTb6InKpCLaWlGHQeXudjhLmA',
    authDomain: 'matesze-hu.firebaseapp.com',
    databaseURL: 'https://matesze-hu.firebaseio.com',
    projectId: 'matesze-hu',
    storageBucket: 'matesze-hu.appspot.com',
    messagingSenderId: '619995381151',
    appId: '1:619995381151:web:632a746bd185b8c34c11aa',
    measurementId: 'G-0LS12GP9M8'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
